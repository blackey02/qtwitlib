#QTwitLib

##Last Update
Feb 4, 2010

##Summary
QTwitLib was the first C++ Twitter library, currently deprecated.

##Building
* Checkout the repository.
* Qt4 must be installed (currently 4.4.0)
* Linux/windows/Mac can be used (currently Ubuntu 8.04/Windows XP sp2/OSX)
* From the root directory type command 'qmake'
* From the root directory...
* Type command 'make' (linux/mac)
* A visual studio .vproj file will be created (windows)
* The lib will be produced into...
* "rootdir"/release (linux/mac)
* "rootdir"/debug (windows)